import vue from 'rollup-plugin-vue'

export default {
  input: 'src/js/CarouselProvider.vue',
  output: {
    format: 'esm',
    file: 'dist/CarouselProvider.js'
  },
  plugins: [
    vue()
  ]
}