@props([
    'dotClass' => '',
    'dotActiveClass' => 'isActive'
])

<div
    {{ $attributes->merge(['class' => 'carousel__dots']) }}
    aria-label="Paginering för slider"
>
    <button
        v-for="i in CarouselProvider.numberSlides"
        v-bind:key="i"
        aria-controls="carousel__slidesHolder"
        v-bind:aria-label="'Gå till slide ' + i"
        type="button"
        class="carousel__dot {{ $dotClass }}"
        v-bind:class="{ '{{ $dotActiveClass }}': CarouselProvider.currentSlide === i }"
        @click="CarouselProvider.$goTo(i - 1)"
    >
        {!! $slot ?? null !!}
    </button>
</div>
