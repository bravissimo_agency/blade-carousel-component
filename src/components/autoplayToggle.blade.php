@props([
    'playingClass' => 'isPlaying',
    'pausedClass' => 'isPaused',
])

<button
    :class="[CarouselProvider.isPaused ? '{{ $pausedClass }}' : '{{ $playingClass }}']"
    type="button"
    @click="CarouselProvider.$toggleAutoplay"
    {{ $attributes }}
>
    <template v-if="!CarouselProvider.isPaused">
        {{ $playing ?? null }}
    </template>

    <template v-if="CarouselProvider.isPaused">
        {{ $paused ?? null }}
    </template>

    {{ $slot ?? null }}
</button>
