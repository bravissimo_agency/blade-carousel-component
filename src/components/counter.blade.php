@props([
    'currentClass' => '',
    'totalClass' => '',
    'seperatorClass' => '',
    'seperatorText' => '/',
    'forceTwoDigits' => false
])

<div {{ $attributes }}>
    @if (empty($current))
        <span 
            class="{{ $currentClass }}"
            v-text="{{ !empty($forceTwoDigits) ? 'CarouselProvider.$forceTwoDigits(CarouselProvider.currentSlide)' : 'CarouselProvider.currentSlide' }}"
        ></span>
    @else
        {!! $current !!}
    @endif

    @if (empty($seperator))
        <span class="{{ $seperatorClass }}">
            {{ $seperatorText }}
        </span>
    @else
        {!! $seperator !!}
    @endif

    @if (empty($total))
        <span 
            class="{{ $totalClass }}"
            v-text="{{ !empty($forceTwoDigits) ? 'CarouselProvider.$forceTwoDigits(CarouselProvider.numberSlides)' : 'CarouselProvider.numberSlides' }}"
        ></span>
    @else
        {!! $total !!}
    @endif
</div>