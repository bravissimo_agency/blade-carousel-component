@props([
    'direction' => 'next'
])

<button 
    type="button"
    @click="CarouselProvider.${{ $direction }}()"
    {{ $attributes }}
>
    {!! $slot !!}
</button>