<div {{ $attributes }}>
    <div
        id="carousel__slidesHolder"
        class="carousel__slidesHolder"
    >
        {!! $slot !!}
    </div>
</div>
