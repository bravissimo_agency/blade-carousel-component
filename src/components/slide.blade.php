@props([
    'activeClass' => 'isActive',
    'inactiveClass' => '',
    'index',
])

<div>
    <div
        :class="{ 
            '{{ $activeClass }}': CarouselProvider.currentSlide === {{ $index + 1 }},
            '{{ $inactiveClass }}': CarouselProvider.currentSlide !== {{ $index + 1 }}
        }"
        {{ $attributes }}
    >
        {!! $slot !!}
    </div>
</div>