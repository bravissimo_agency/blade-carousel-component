<?php

namespace Bravissimo\BladeCarouselComponent;

use Illuminate\Support\ServiceProvider;

class BladeCarouselComponentServiceProvider extends ServiceProvider {
    public function boot() {
        $this->loadViewsFrom(__DIR__, 'carousel');
    }
}
